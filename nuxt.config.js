export default {
  // Target: https://go.nuxtjs.dev/config-target
  target: "static",

  // Global page headers: https://go.nuxtjs.dev/config-head
  server: {
    port: 3002,
    host: "0.0.0.0",
  },
  head: {
    title: "Signal24",
    htmlAttrs: {
      lang: "en",
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content:
          'Прокуратура города Алматы совместно с Национальной палатой предпринимателей \'"Атамекен"',
      },
      { name: "format-detection", content: "telephone=no" },
      { hid: "og:title", name: "og:title", content: "signal24" },
      { hid: "og:url", name: "og:url", content: "https://signal24.kz" },
      { hid: "og:image", name: "og:image", href: "/images/gerb.png" },
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/images/gerb.png" }],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ["~/assets/styles/main.scss"],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [{ src: "~/plugins/vue-the-mask.js", ssr: false }],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: ["@nuxtjs/axios", "nuxt-i18n"],
  i18n: {
    locales: ["kz", "ru"],
    defaultLocale: "ru",
    strategy: "no_prefix",
    vueI18n: {
      fallbackLocale: "ru",
      messages: {
        ru: {
          welcome: `Вас приветствует мобильная группа по защите <br />
          прав и интересов`,
          welcomeFrom: `Прокуратура города Алматы совместно с <br /> Национальной палатой предпринимателей "Атамекен".`,
          signalBtnText: `Оформить сигнал`,
          checkLastSignal: `Проверить статус последнего сигнала`,
          phoneNumber: `Номер телефона`,
          enterPhoneNumber: `Введите номер телефона`,
          signalNumber: `Номер заявки`,
          status: `Статус`,
          formTitle: `Оформление сигнала`,
          fio: `ФИО`,
          requiredField: `Обязательное поле`,
          contactNumber: `Контактный номер`,
          address: `Точный адрес`,
          exampleAddress: `Например: проспект Достык 134, дом 6, кв. 8`,
          symbols: `символов`,
          file: `Файл`,
          selectFile: `Выберите файл`,
          unselectedFile: `Файл не выбран`,
          businessName: `Название юр. лица или ИП`,
          enterBusinessName: `Введите название субъекта`,
          region: `Район`,
          problem: `Проблема`,
          describeProblem: `Опишите проблему`,
          agreement: `Даю согласие на сбор и обработку персональных данных`,
          sendSignal: `Отправить сигнал`,
          cancel: `Отмена`,
          notification: `Уведомление`,
          close: `Закрыть`,
          new: `Новые`,
          inProgress: `В работе`,
          success: "Завершенные",
          search: "Поиск",
          exit: "Выйти",
          no_file: "Файла нету",
        },
        kz: {
          welcome: `Сізді бизнес құқықтарын қорғау жөніндегі <br />
            мобильді топ қарсы алады!`,
          welcomeFrom: `Алматы қаласының прокуратурасы “Атамекен” Ұлттық <br /> Кәсіпкерлер палатасымен бірлесіп`,
          signalBtnText: `Сигнал рәсімдеу`,
          checkLastSignal: `Соңғы сигналдың күйін тексеру`,
          phoneNumber: `Телефон нөмірі`,
          enterPhoneNumber: `Телефон нөмірін енгізіңіз`,
          signalNumber: `Өтініш нөмірі`,
          status: `Статус`,
          formTitle: `Сигнал рәсімделуі`,
          fio: `Аты, әкесінің аты, тегі`,
          requiredField: `Міндетті өріс`,
          contactNumber: `Байланыс нөмірі`,
          address: `Нақты мекенжай`,
          exampleAddress: `Мысалы: Достық даңғылы 134, 6 корпус, пәтер. 8`,
          symbols: `таңба`,
          file: `Файл`,
          selectFile: `Файлды таңдаңыз`,
          unselectedFile: `Файл таңдалмаған`,
          businessName: `ТОО немесе ИП атауы`,
          enterBusinessName: `Субъект атауын енгізіңіз`,
          region: `Аудан`,
          problem: `Мәселе`,
          describeProblem: `Мәселені сипаттаңыз`,
          agreement: `Мен жеке деректерді жинауға және өңдеуге келісемін`,
          sendSignal: `Сигнал жіберу`,
          cancel: `Болдырмау`,
          notification: `Хабарландыру`,
          close: `Жабу`,
          new: `Жаңа`,
          inProgress: `Орындалуда`,
          success: "Аяқталды",
          search: "Іздеу",
          exit: "Шығу",
          no_file: "Файл жоқ",
        },
      },
    },
    detectBrowserLanguage: {
      useCookie: true,
      cookieKey: "i18n_redirected",
    },
  },
  axios: {
    //http://192.168.0.90:8000
    // baseURL: 'http://192.168.172.66:8000/api/v1/',
    baseURL: "https://signal24.kz",
    // baseURL: "http://192.168.0.90:8000/",
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},
};
