FROM node:14.18.0

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY . .

RUN npm run build & npm run generate



ENV PORT 3002

EXPOSE 3002

CMD [ "npm", "run", "start" ]
