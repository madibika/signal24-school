const districts = [
  {
    name: "Алатауский",
  },
  {
    name: "Алмалинский",
  },
  {
    name: "Ауэзовский",
  },
  {
    name: "Бостандыкский",
  },
  {
    name: "Жетысуский",
  },
  {
    name: "Медеуский",
  },
  {
    name: "Наурызбайский",
  },
  {
    name: "Турксибский",
  },
];

export default districts;
